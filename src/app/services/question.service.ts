import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageService } from './storage.service'
import { answer } from '../models/answer';

const ENDPOINT_API = "https://training-homework.calllab.net/v1";

@Injectable({
    providedIn: 'root'
  })

export class QuestionService {
    constructor(private client: HttpClient, private storageService : StorageService) { }
   

  getCategories(): Observable<any> {
    const header = new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.storageService.getUser().accessToken}`
    })
    const headers = { 'Authorization': `Bearer ${this.storageService.getUser().accessToken}` };
    return this.client.get(
      ENDPOINT_API + '/questions/categories',
      { 
        headers : new HttpHeaders({ 
        "Content-Type" : "application/json" ,
        "Authorization": `Bearer ${this.storageService.getUser().accessToken}` })
      }
      
    );
  }

  getCategoriesById(id : String): Observable<any> {
    const header = new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.storageService.getUser().accessToken}`
    })
    const headers = { 'Authorization': `Bearer ${this.storageService.getUser().accessToken}` };
    return this.client.get(
      ENDPOINT_API + '/questions/categories/'+ id,
      { 
        headers : new HttpHeaders({ 
        "Content-Type" : "application/json" ,
        "Authorization": `Bearer ${this.storageService.getUser().accessToken}` })
      }
      
    );
  }

  submitAnswer(answer : answer ) : Observable<any> {
    const header = new Headers({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.storageService.getUser().accessToken}`
    })
    const headers = { 'Authorization': `Bearer ${this.storageService.getUser().accessToken}` };
    return this.client.post(
      ENDPOINT_API + '/questions/submit-assignment',
      answer,
      { 
        headers : new HttpHeaders({ 
        "Content-Type" : "application/json" ,
        "Authorization": `Bearer ${this.storageService.getUser().accessToken}` })
      }
      
    );
  }
}