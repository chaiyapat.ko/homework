import { Component } from '@angular/core';
import { QuestionService } from '../services/question.service';
import { questionCategories } from '../models/questionCategories';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrl: './question.component.css',
})
export class QuestionComponent {
  public questionCategories: questionCategories[] = [];
  constructor(
    private questionService: QuestionService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.questionService.getCategories().subscribe({
      next: (respone) => {
        if (respone.data != null) {
          this.questionCategories = respone.data;
        }
      },
      error: (err) => {
        this.dialog.open(ErrorDialogComponent, {
          data: { message: err.message },
        });
        this.router.navigateByUrl('/login');
      },
    });
  }
}
