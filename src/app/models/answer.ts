export interface answer {
    questionCategoryId: string,
    questions: answerData[],
}
export interface answerData {
    questionId: string,
    answers: answerList[]
}
export interface answerList {
    questionAnswerId: string
}

export interface score {
    fullScore: number
    score:number
}