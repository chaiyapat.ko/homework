import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MatDialog,MatDialogRef } from "@angular/material/dialog";
import { ErrorDialogComponent } from "../error-dialog/error-dialog.component";
import { StorageService } from '../services/storage.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  
  loginObj: LoginModel  = new LoginModel();
  constructor(private router: Router, private authService: AuthService,  private dialog: MatDialog, private localStorage: StorageService){}

  onLogin() {
    this.authService.login(this.loginObj.username, this.loginObj.password).subscribe({
      next: data => {
        console.log(data);
        alert("Login Success...");
        this.localStorage.saveUser(data.data);
        this.router.navigateByUrl('/question');
      },
      error: err => {
        console.error(err);
        this.dialog.open(ErrorDialogComponent, { 
          data: { message: err.message }
        });
      }
    });
  }


}


export class LoginModel  { 
  username: string;
  password: string;

  constructor() {
    this.username = ""; 
    this.password= ""
  }

}


