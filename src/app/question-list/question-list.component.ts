import { Component } from '@angular/core';
import { QuestionService } from '../services/question.service';
import { questionList } from '../models/questionList';
import { questionCategories } from '../models/questionCategories';
import { ActivatedRoute,Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { answer , answerData } from '../models/answer';
import { TimerService } from '../services/time.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrl: './question-list.component.css',
})
export class QuestionListComponent {
  public questionList!: questionList;
  questionInfo: any = {};
  answer = {} as answer;
  answerData : answerData[] = [];
  timerSubscription!: Subscription;
  time: string = '00:00';
  resultAnswer!: answer;
  Score!:String;
  allScore!:String;
  isShowScore : boolean = false;
  constructor(
    private questionService: QuestionService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private timerService : TimerService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      
      this.questionService.getCategoriesById(params.get('id')!).subscribe({
        next: (respone) => {
          if (respone.data != null) {
            this.questionList = respone.data;
            this.questionInfo = this.questionList.questionInfo[0]
            this.startTimer(this.questionList.timeLimitOfMinuteUnit)
            console.log(this.questionList);
           
          } else {
            alert("QuestList Not Found...");
          }
        },
        error: (err) => {
          this.dialog.open(ErrorDialogComponent, {
            data: { message: err.message },
          });
          this.router.navigateByUrl('/login');
        },
      });
    });


  }
  next(index : number) {
   
    this.questionInfo = this.questionList.questionInfo[index];
  }
  back(index : number) {
   
    this.questionInfo = this.questionList.questionInfo[index -2];
    if (this.questionList.questionInfo && this.questionList.questionInfo.length > 0) {
      const currentQuestionId = this.questionList.questionInfo[index - 2].questionId;
     
      this.answerData = this.answerData.filter(item => item.questionId !== currentQuestionId);
      console.log(this.answerData);
    }
  }
  
  onCheckboxChange(event: any, item: any) {
    const questionIndex = this.answerData.findIndex((q) => q.questionId === event.target.name);
    if(event.target.checked){
      if (questionIndex === -1) {
        this.answerData.push({ questionId: event.target.name, answers: [{ questionAnswerId: item.questionAnswerId }] });
      } else {
        this.answerData[questionIndex].answers.push({ questionAnswerId: item.questionAnswerId });
      }
    } else {
      if (questionIndex > -1) {
        const answerIndex = this.answerData[questionIndex].answers.findIndex((a) => a.questionAnswerId === item.questionAnswerId);
        if (answerIndex > -1) {
          this.answerData[questionIndex].answers.splice(answerIndex, 1);
        }
       
        if (this.answerData[questionIndex].answers.length === 0) {
          this.answerData.splice(questionIndex, 1);
        }
      }
    }
   console.log(this.answerData)
  }

  submitAnswer(){
    
    this.resultAnswer = {
      questionCategoryId: this.questionList.questionCategoryId,
      questions: this.questionList.questionInfo.map(question => {
        const foundItem = this.answerData.find(item => item.questionId === question.questionId);
        return {
          questionId: question.questionId,
          answers: foundItem ? foundItem.answers : []
        };
      })
    };
 
    this.questionService.submitAnswer(this.resultAnswer).subscribe({
      next: (respone) => {
        if (respone.data != null) {
          this.Score = respone.data.score;
          this.allScore = respone.data.fullScore;
          this.isShowScore = true;
        }
        console.log(respone);
      },
      error: (err) => {
        this.dialog.open(ErrorDialogComponent, {
          data: { message: err.message },
        });
        this.router.navigateByUrl('/login');
      },
    });
  }

  isAnswer(n: number): boolean {
    const questionIndex = this.answerData.findIndex((q) => q.questionId === this.questionInfo.questionId);
    return questionIndex > -1 && this.answerData[questionIndex].answers.length > 0;
  }

  startTimer(limit: number) {
    this.timerSubscription = this.timerService.startCountdown(limit).subscribe(time => {
      this.time = time;
    });
    this.timerService.onLimitReached().subscribe(() => {
      this.showErrorPopup();
    });
  }

  showErrorPopup() {
    const dialogRef = this.dialog.open(ErrorDialogComponent, {
      data: { message: 'หมดเวลา !!!' },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.isShowScore = true;
      this.submitAnswer();
    });

  }

  ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
    this.time = '00:00:00';
  }

  backQuestion() {
    this.isShowScore = false;
    this.router.navigateByUrl('/question');
  }
}
